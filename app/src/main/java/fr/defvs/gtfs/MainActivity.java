package fr.defvs.gtfs;

import android.widget.*;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
	implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, LocationListener
{
	private RadioButton radioAller;
	private RadioButton radioRetour;
	private boolean gpsReady;
	private GoogleApiClient googleApiClient;
	private LocationRequest mLocationRequest;
	private Location lastLocation;
	private static TextView lat;
	private static TextView lon;
	private ViewPager mapPager;
	private Fragment[] fragments;
	private MapFrag mapFrag;
	private boolean arEnabled = false;
	private boolean arState;
	static final boolean COMING = false;
	static final boolean GOING = true;
	private StopListFragment stopListFrag;
	private static final String FILE = "stopSave.stp";

	private FloatingActionButton add;

	@Override
	protected void onStart()
	{
		googleApiClient.connect();
		super.onStart();
	}

	@Override
	protected void onStop()
	{
		googleApiClient.disconnect();
		super.onStop();
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		Toolbar t = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(t);
		getSupportActionBar().setLogo(R.drawable.ic_launcher);
		getSupportActionBar().setTitle("");
		
		gpsReady = false;
		permissions();
		
		lat = (TextView) findViewById(R.id.latText);
		lon = (TextView) findViewById(R.id.lonText);
		
		if (googleApiClient == null) {
			googleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
		}
		
		CheckBox radiocheck = (CheckBox) findViewById(R.id.radiocheck);
		RadioGroup radiogr = (RadioGroup) findViewById(R.id.radiogr);
		final ToggleButton modeSwitch = (ToggleButton) findViewById(R.id.modeSwitch);
		radioAller = (RadioButton) findViewById(R.id.radioAller);
		radioRetour = (RadioButton) findViewById(R.id.radioRetour);
		radioAller.setChecked(true);
		final EditText stopNameField = (EditText) findViewById(R.id.stopName);
		stopNameField.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					boolean handled = false;
					if (actionId == EditorInfo.IME_ACTION_SEND) {
						add.performClick();
						handled = true;
					}
					return handled;
				}
			});
		
		radiocheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton p1, boolean p2)
				{
					enableCheckbox(p2);
					arEnabled = p2;
				}
				
			
		});
		arState = GOING;
		
		radiogr.setOnCheckedChangeListener(new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(RadioGroup p1, int p2)
				{
					getActionBar();
					if (p2 == R.id.radioAller) arState = GOING;
					else arState = COMING;
				}
				
			
		});
		
		modeSwitch.setChecked(true);
		
		modeSwitch.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton p1, boolean p2)
				{
					if(!p2){
						Snackbar.make(modeSwitch,"Long click on the map to select a position",Snackbar.LENGTH_SHORT).show();
					}else{
						if(mapFrag.lastMarker != null)
							mapFrag.lastMarker.remove();
						mapFrag.lastMarker = null;
					}
					mapFrag.setGpsMode(p2);
				}
		});
		
		//Viewpager
		mapFrag = new MapFrag();
		stopListFrag = new StopListFragment();
		fragments = new Fragment[]{mapFrag,stopListFrag};
		
		stopListFrag.pushFragments(mapFrag);
		stopListFrag.pushRef(this);
		
		mapPager = (ViewPager) findViewById(R.id.mapPager);
		SupportMapFragment f = SupportMapFragment.newInstance();
		
		mapPager.setAdapter(new MapPagerAdapter(getSupportFragmentManager()));
		
		mapFrag.pushButton(modeSwitch);
		
		stopListFrag.stopList = Serializer.readSerializable(this,FILE);
		
		// add button
		add = (FloatingActionButton) findViewById(R.id.floatSave);
		add.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1)
				{
					if(mapFrag.gpsMode && gpsReady){
						if (lastLocation != null){
							if(stopListFrag.stopList == null) stopListFrag.stopList = new ArrayList<Stops>();
							String stopName = stopNameField.getText().toString();
							if(stopNameField.getText().length() == 0){
								Snackbar.make(modeSwitch,"No name entered",Snackbar.LENGTH_SHORT).show();
								return;
							}
							if (arEnabled){
								stopListFrag.stopList.add(new Stops(lastLocation,stopName,arState));
							}else{
								stopListFrag.stopList.add(new Stops(lastLocation,stopName));
							}
							Snackbar.make(modeSwitch, "Stop saved",Snackbar.LENGTH_SHORT).show();
							stopListFrag.updateList();
							saveList(stopListFrag.stopList,MainActivity.this,FILE);
							stopNameField.setText("");
							
						}else Snackbar.make(modeSwitch, "Location unavailable",Snackbar.LENGTH_SHORT).show();
					}
					if(!mapFrag.gpsMode || !gpsReady){
						if(mapFrag.selectedLatLng != null){
							if(stopListFrag.stopList == null) stopListFrag.stopList = new ArrayList<Stops>();
							if(stopNameField.getText().toString() == ""){
								Snackbar.make(modeSwitch,"No name entered",Snackbar.LENGTH_SHORT);
								return;
							}
							if(arEnabled){
								stopListFrag.stopList.add(new Stops(mapFrag.selectedLatLng,stopNameField.getText().toString(),arState));
							}else{
								stopListFrag.stopList.add(new Stops(mapFrag.selectedLatLng,stopNameField.getText().toString()));
							}
							Snackbar.make(modeSwitch, "Stop saved",Snackbar.LENGTH_SHORT).show();
							stopListFrag.updateList();
							saveList(stopListFrag.stopList,MainActivity.this,FILE);
							stopNameField.setText("");
							if(mapFrag.lastMarker != null){
								mapFrag.lastMarker.remove();
								mapFrag.lastMarker = null;
								mapFrag.selectedLatLng = null;
							}
						}else Snackbar.make(modeSwitch,"Select something on the map first",Snackbar.LENGTH_SHORT).show();
					}
				}
				
		});
		
		Spinner navSpinner = (Spinner) findViewById(R.id.toolbarSpinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
																			 R.array.toolbar_tabs, R.layout.spinner);
// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		navSpinner.setAdapter(adapter);
		navSpinner.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4)
				{
					// TODO: Implement this method
					switch(p3){
						
					}
				}
				
			
		});
    }
	
	private void enableCheckbox(boolean enabled){
		radioAller.setEnabled(enabled);
		radioRetour.setEnabled(enabled);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// TODO: Implement this method
		return super.onCreateOptionsMenu(menu);
		
	}
	
	
	
	
	private void saveList(ArrayList l, Context c,@Nullable String f){
		if (f == null)
			f = FILE;
		Serializer.saveSerializable(c,l,f);
	}
	
	private void permissions(){
		if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
			gpsReady = true;
			return;
		}else{
			ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},1);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
	{
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults.length > 0
			&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
			// perm ok, return
			gpsReady = true;
			onConnected(null);
		}else{
			gpsReady = false;
			
		}
	}
	
	//GoogleApi Callbacks
	@Override
	public void onConnectionFailed(ConnectionResult p1)
	{
		// TODO: Implement this method
		lat.setText("Google Fused Location unavailable");
		lon.setText("");
	}


	@Override
	public void onConnected(Bundle p1)
	{
		if(gpsReady){
			createLocationRequest();
			startLocationUpdates();
		}else{
			lat.setText("Location perms denied");
			lon.setText("");
		}
		MapsInitializer.initialize(this);
	}

	@Override
	public void onConnectionSuspended(int p1)
	{
		// TODO: Implement this method
	}
	
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(500);
		mLocationRequest.setFastestInterval(250);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}
	
	@Override
	public void onLocationChanged(Location p1)
	{
		lastLocation = p1;
		if(mapFrag.gpsMode) updateLocationUi(p1);
		// mapFrag.changeCamera(CameraUpdateFactory.newLatLng(
			// new LatLng(p1.getLatitude(),p1.getLongitude())));
		// mapFrag.onLocationUpdated(p1);
	}
	
	protected void startLocationUpdates() {
		LocationServices.FusedLocationApi.requestLocationUpdates(
            googleApiClient, mLocationRequest, this);
	}
	
	private void updateLocationUi(Location loc){
		lat.setText("Lat : " + String.valueOf(loc.getLatitude()));
		lon.setText("Lon : " + String.valueOf(loc.getLongitude()));
	}
	
	private class MapPagerAdapter extends FragmentStatePagerAdapter
	{
		public MapPagerAdapter(FragmentManager fm) {
            super(fm);
        }

		@Override
		public int getCount()
		{
			// TODO: Implement this method
			return fragments.length;
		}

		@Override
		public Fragment getItem(int p1)
		{
			// TODO: Implement this method
			return fragments[p1];
		}
		
		
	}
	public static class StopListFragment extends Fragment
	{
		ListView r;
		ArrayList<Stops> stopList;
		MapFrag m;
		MainActivity act;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// TODO: Implement this method
			return inflater.inflate(R.layout.stops,container,false);
		}
		
		void pushFragments(MapFrag map){
			m = map;
		}
		void pushRef(MainActivity m){
			act = m;
		}

		@Override
		public void onViewCreated(View v, Bundle savedInstanceState)
		{
			// TODO: Implement this method
			super.onViewCreated(v, savedInstanceState);
			
			r = (ListView) v.findViewById(R.id.stopsListView);
			r.setAdapter(new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1));
			r.setOnItemClickListener(new OnItemClickListener(){

					@Override
					public void onItemClick(AdapterView<?> p1, View p2, int i, long p4)
					{
						final int ii = i;
						
						PopupMenu p = new PopupMenu(getContext(),p2);
						p.inflate(R.menu.delete_popup);
						p.show();
						p.setOnMenuItemClickListener(new OnMenuItemClickListener(){

								@Override
								public boolean onMenuItemClick(MenuItem p1)
								{
									switch(p1.getItemId()){
										case R.id.popup_showmap:{
												LatLng locat = new LatLng(stopList.get(ii).getLat(),stopList.get(ii).getLon());
											m.addLocation(locat,BitmapDescriptorFactory.HUE_MAGENTA);
											m.changeCamera(CameraUpdateFactory.newLatLng(locat));
											act.mapPager.setCurrentItem(0);
											return true;
										}
										case R.id.popup_rename:{
											AlertDialog d;
											final EditText e = new EditText(getContext());
											d = new AlertDialog.Builder(getContext())
												.setTitle("Rename")
													.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){

														@Override
														public void onClick(DialogInterface p1, int p2)
														{
															Stops old = stopList.get(ii);
															old.setName(e.getText().toString());
															stopList.set(ii,old);
															p1.dismiss();
															updateList();
														}
												})
												.setView(e)
												.create();
											
											e.setEms(15);
											e.setText(stopList.get(ii).getName());
											d.show();
											return true;
										}
										case R.id.popup_delete:{
												final AlertDialog d = new AlertDialog.Builder(getContext())
													.setTitle("Delete stop \"" + stopList.get(ii).getName() + "\" ?")
													.setMessage("This can\'t be undone !")
													.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener(){

														@Override
														public void onClick(DialogInterface p1, int p2)
														{
															
															p1.dismiss();
															stopList.remove(ii);
															updateList();
														}


													})
													.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener(){

														@Override
														public void onClick(DialogInterface p1, int p2)
														{
															p1.dismiss();
														}


													})
													.create();
												d.show();
											return true;
										}
										default:
											return false;
									}
								}
								
							
						});
					}
					
				
			});
		}
		
		public void updateList(){
			String[] list = new String[stopList.size()];
			for(int i = 0;i < stopList.size();i++){
				list[i] = stopList.get(i).getName() + "\n" +
					"Lat|Lon: " + String.valueOf(stopList.get(i).getLat())
					+ " | "+ String.valueOf(stopList.get(i).getLon());
					if  (stopList.get(i).isAr())
						list[i] = list[i] + "\n" + (stopList.get(i).isArState() ? "Going" : "Coming");
			}
			ArrayAdapter a = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,list);
			r.setAdapter(a);
			a.notifyDataSetChanged();
		}
		
	}
	
	public static class MapFrag extends Fragment implements OnMapReadyCallback
	{
		boolean ready = false;
		GoogleMap gMap;
		private Location location;
		boolean gpsMode = true;
		ToggleButton gpsButton;
		Marker lastMarker;

		public void setGpsMode(boolean p2)
		{
			gpsMode = p2;
		}
		
		void pushButton(ToggleButton b){
			gpsButton = b;
		}

		public void addLocation(LatLng latlng,float hue)
		{
			if (lastMarker != null){ lastMarker.setPosition(latlng); lastMarker.setIcon(BitmapDescriptorFactory.defaultMarker(hue));}
			else {Marker m = gMap.addMarker(new MarkerOptions().position(latlng)
				.flat(false).icon(BitmapDescriptorFactory.defaultMarker(hue)));
				lastMarker = m;}
		}
		
		LatLng selectedLatLng;

		@Override
		public void onMapReady(GoogleMap gMap)
		{
			this.gMap = gMap;
			gMap.setMyLocationEnabled(true);
			ready = true;
			
			gMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener(){

					@Override
					public void onMapLongClick(LatLng p1)
					{
						lat.setText("Lat : " + String.valueOf(p1.latitude));
						lon.setText("Lon : " + String.valueOf(p1.longitude));
						selectedLatLng = p1;
						addLocation(p1,BitmapDescriptorFactory.HUE_RED);
						gpsButton.setChecked(false);
						gpsMode = false;
					}
				
			});
			MapsInitializer.initialize(getContext());
		}
		
		public void changeCamera(CameraUpdate c){
			if(ready) gMap.moveCamera(c);
		}
		
		MapView map;

		private final static String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// TODO: Implement this method
			return inflater.inflate(R.layout.map,container,false);
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState)
		{
			// TODO: Implement this method
			super.onViewCreated(view, savedInstanceState);
			
			Bundle mapViewBundle = null;
			if (savedInstanceState != null) {
				mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
			}
			map = (MapView) view.findViewById(R.id.map);
			map.onCreate(mapViewBundle);

			map.getMapAsync(this);
		}

		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			// TODO: Implement this method
			super.onCreate(savedInstanceState);
		}

		@Override
		public void onDestroy()
		{
			// TODO: Implement this method
			super.onDestroy();
			map.onDestroy();
		}

		@Override
		public void onPause()
		{
			// TODO: Implement this method
			super.onPause();
			map.onPause();
		}

		@Override
		public void onResume()
		{
			// TODO: Implement this method
			super.onResume();
			map.onResume();
		}

		@Override
		public void onStop()
		{
			// TODO: Implement this method
			super.onStop();
			map.onStop();
		}

		@Override
		public void onStart()
		{
			// TODO: Implement this method
			super.onStart();
			map.onStart();
		}

		@Override
		public void onSaveInstanceState(Bundle outState)
		{
			// TODO: Implement this method
			super.onSaveInstanceState(outState);
			
			Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
			if (mapViewBundle == null) {
				mapViewBundle = new Bundle();
				outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
			}

			map.onSaveInstanceState(mapViewBundle);
		}

		@Override
		public void onLowMemory()
		{
			// TODO: Implement this method
			super.onLowMemory();
			map.onLowMemory();
		}
		
		
		
	}
}
