package fr.defvs.gtfs;
import android.location.Location;
import com.google.android.gms.maps.model.LatLng;

public class Stops
{
	private double lat;
	private double lon;
	private String name;
	private boolean arState;
	private boolean arEnabled;
	
	public Stops(LatLng pos, String n){
		lat = pos.latitude;
		lon = pos.longitude;
		name = n;
		arEnabled = false;
	}
	public Stops(Location pos, String n){
		lat = pos.getLatitude();
		lon = pos.getLongitude();
		name = n;
		arEnabled = false;
	}
	public Stops(LatLng pos, String n,boolean arState){
		lat = pos.latitude;
		lon = pos.longitude;
		name = n;
		this.arState = arState;
		arEnabled = true;
	}
	public Stops(Location pos, String n,boolean arState){
		lat = pos.getLatitude();
		lon = pos.getLongitude();
		name = n;
		this.arState = arState;
		arEnabled = true;
	}

	public boolean isAr()
	{
		return arEnabled;
	}

	public void setArState(boolean arState)
	{
		this.arState = arState;
	}

	public boolean isArState()
	{
		return arState;
	}

	public void setLon(double lon)
	{
		this.lon = lon;
	}

	public double getLon()
	{
		return lon;
	}

	public void setLat(double lat)
	{
		this.lat = lat;
	}

	public double getLat()
	{
		return lat;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
