package fr.defvs.gtfs;
import android.graphics.Color;

public class Line
{
	public static final int TYPE_STREET = 1;
	public static final int TYPE_UNDERGROUND = 2;
	public static final int TYPE_RAIL = 3;
	public static final int TYPE_BUS = 4;
	public static final int TYPE_CABLE = 5;
	public static final int TYPE_SUSPENDED = 6;
	public static final int TYPE_FUNICULAR = 7;
	
	private String shortName;
	private String longName;
	private int type;
	private String color;
	
	public Line(String shortName,String longName,int type, String color){
		this.shortName = shortName;
		this.longName = longName;
		this.type = type;
		this.color = color;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public String getShortName()
	{
		return shortName;
	}

	public void setLongName(String longName)
	{
		this.longName = longName;
	}

	public String getLongName()
	{
		return longName;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getType()
	{
		return type;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getColor()
	{
		return color;
	}
}
